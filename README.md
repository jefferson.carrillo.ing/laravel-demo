#DOCUMENTACION OFICIAL DE LARAVEL
https://laravel.com/docs/8.x

#VERSIONES
LARAVEL=> 8.36.2
PHP=> 7.4.16
POSTGREST => 10.8

Aqui el link directo del paquete xampp con las versiones adecuadas para este proyecto
https://www.apachefriends.org/xampp-files/8.0.3/xampp-windows-x64-8.0.3-0-VS16-installer.exe

Aqui el link directo dependecias composer
https://getcomposer.org/download/Composer-Setup.exe


#INSTRUCCIONES

1.-Ejecutar en la base de datos el escript sql llamado "BASE_DE_DATOS.SQL" para crear las tablas y datos por default requeridos.

2.-modificar el archivo .env donde apartir de la linea 10 se debe modificar la cadena de conexion a la base de datos con sus credenciales.

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=LARAVEL-DEMO
DB_USERNAME=su usuario
DB_PASSWORD=su clave

3.- Arrancar el proyecto con
php artisan serve

4.- puede proceder a crearse un usuario o usar el usuario administrador

usuario: administrador@gmail.com
clave: 123456789
