
CREATE DATABASE "LARAVEL-DEMO";

CREATE TABLE "users" (
"id" serial4 NOT NULL,
"id_rol" int4,
"name" varchar(255),
"email" varchar(255),
"email_verified_at" timestamp(255),
"password" varchar(255),
"remember_token" varchar(255),
"status" int4,
"created_at" timestamp(0) DEFAULT  timezone('America/Guayaquil',now()),
"updated_at" timestamp(0) DEFAULT  timezone('America/Guayaquil',now()),
"two_factor_seret" text,
"two_factor_recovery_codes" text,
PRIMARY KEY ("id") ,
CONSTRAINT "users_email_unique" UNIQUE ("email")
)
WITHOUT OIDS;
CREATE TABLE "password_resets" (
"email" varchar(255),
"token" varchar(255),
"created_at" timestamp(255)
)
WITHOUT OIDS;
CREATE TABLE "failed_jobs" (
"id" serial4 NOT NULL,
"connection" text,
"queue" text,
"payload" text,
"exeption" text,
"failed_at" timestamp(255),
PRIMARY KEY ("id")
)
WITHOUT OIDS;
CREATE TABLE "migrations" (
"id" int4 NOT NULL,
"migration" varchar,
"bath" int4,
PRIMARY KEY ("id")
)
WITHOUT OIDS;
CREATE TABLE "tareas" (
"id" serial4 NOT NULL,
"id_estado" int4,
"id_usuario" int4,
"nombre" varchar(200),
"descripcion" varchar(500),
"fecha_fin" timestamp(0),
"status" int4,
"created_at" timestamp(0) DEFAULT  timezone('America/Guayaquil',now()),
"updated_at" timestamp(0) DEFAULT  timezone('America/Guayaquil',now()),
PRIMARY KEY ("id")
)
WITHOUT OIDS;
CREATE TABLE "historiales" (
"id" serial4 NOT NULL,
"id_tarea" int4,
"id_usuario" int4,
"id_estado" int4,
"fecha" timestamp(0),
"created_at" timestamp,
"updated_at" timestamp,
PRIMARY KEY ("id")
)
WITHOUT OIDS;
CREATE TABLE "estados" (
"id" serial4 NOT NULL,
"nombre" varchar(200),
"descripcion" varchar(500),
"status" int4,
"created_at" timestamptz(0) DEFAULT  timezone('America/Guayaquil',now()),
"updated_at" timestamptz(0) DEFAULT  timezone('America/Guayaquil',now()),
PRIMARY KEY ("id")
)
WITHOUT OIDS;
CREATE TABLE "roles" (
"id" serial4 NOT NULL,
"nombre" varchar(200),
"descripcion" varchar(500),
"status" int4,
"created_at" timestamp DEFAULT  timezone('America/Guayaquil',now()),
"updated_at" timestamp DEFAULT  timezone('America/Guayaquil',now()),
PRIMARY KEY ("id")
)
WITHOUT OIDS;

ALTER TABLE "historiales" ADD CONSTRAINT "fk_historial_estado_1" FOREIGN KEY ("id_estado") REFERENCES "estados" ("id");
ALTER TABLE "tareas" ADD CONSTRAINT "fk_tareas_estado_1" FOREIGN KEY ("id_estado") REFERENCES "estados" ("id");
ALTER TABLE "historiales" ADD CONSTRAINT "fk_historial_users_1" FOREIGN KEY ("id_usuario") REFERENCES "users" ("id");
ALTER TABLE "tareas" ADD CONSTRAINT "fk_tareas_users_1" FOREIGN KEY ("id_usuario") REFERENCES "users" ("id");
ALTER TABLE "historiales" ADD CONSTRAINT "fk_historial_tareas_1" FOREIGN KEY ("id_tarea") REFERENCES "tareas" ("id");
ALTER TABLE "users" ADD CONSTRAINT "fk_users_roles_1" FOREIGN KEY ("id_rol") REFERENCES "roles" ("id");




--ROLES POR DEFECTO
INSERT INTO "roles"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (1, 'ADMINISTRADOR', NULL, 1, '2021-04-13 10:15:18', '2021-04-13 10:15:18');
INSERT INTO "roles"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (2, 'GENERAL', NULL, 1, '2021-04-13 10:15:42', '2021-04-13 10:15:45');

--USUARIO POR DEFECTO
INSERT INTO "users"("id", "id_rol", "name", "email", "email_verified_at", "password", "remember_token", "status", "created_at", "updated_at", "two_factor_seret", "two_factor_recovery_codes") VALUES (1, 1, 'ADMINISTRADOR', 'administrador@gmail.com', NULL, '$2y$10$bBAHJWy7/DQtqfFRM9EoBu4x52W7m0.WKnj5TtYNPa0vb/7EoPH3a', NULL, 1, '2021-04-10 07:31:40', '2021-04-10 07:31:40', NULL, NULL);


--ESTADOS POR DEFECTO
INSERT INTO "estados"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (0, 'Ninguno', 'No se a especificado ninguna regla para este estado.', 1, '2021-04-10 08:23:04+00', '2021-04-10 08:23:04+00');
INSERT INTO "estados"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (1, 'Ejecutándose', 'Tarea cuya fecha fin es menor a la fecha actual.', 1, '2021-04-10 08:23:04+00', '2021-04-10 08:23:04+00');
INSERT INTO "estados"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (2, 'Finalizada', 'Tarea cuya fecha fin es mayor o igual a la fecha actual.', 1, '2021-04-10 08:23:04+00', '2021-04-10 08:23:04+00');
INSERT INTO "estados"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (3, 'Pausada', 'tarea que estando en estado “ejecutándose” explícitamente fue pausada por el propietario de dicha tarea, este tipo de tareas pueden volver al estado “Ejecutandose” siempre y cuando la fecha de fin sea menor a la fecha actual.', 1, '2021-04-10 08:23:04+00', '2021-04-10 08:23:04+00');
INSERT INTO "estados"("id", "nombre", "descripcion", "status", "created_at", "updated_at") VALUES (4, 'Cancelada', 'Tarea que estando en estado “Ejecutandose” fue cancelada explícitamente por el propietario, una tarea en estado Cancelada no puede modificar su estado.', 1, '2021-04-10 08:23:04+00', '2021-04-10 08:23:04+00');


--AJUSTAR SECUENCIAL INCREMENTAL


SELECT  setval('roles_id_seq', ( select COALESCE(max(id) , 1) from roles ));
SELECT  setval('estados_id_seq', ( select COALESCE(max(id) , 1) from estados));
SELECT  setval('users_id_seq', ( select COALESCE(max(id) , 1) from users));
