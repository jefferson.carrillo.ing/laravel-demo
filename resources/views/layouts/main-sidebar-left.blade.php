<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('img/default/users.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p> {{ Auth::user()->name }} </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-th-large"></i>
                    <span>Gestores</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if (Auth::user()->id_rol == 1)
                        <li><a href="{{ url('/admin/estado') }}"><i class="fa fa-tag"></i>Estado</a></li>
                    @endif
                    <li><a href="{{ url('/admin/tarea') }}"><i class="fa fa-tasks"></i>Tarea</a></li>
                    <li><a href="{{ url('/admin/historial') }}"><i class="fa fa-history"></i>Historial</a></li>

                </ul>
            </li>
        </ul>
    </section>
</aside>
