<script>
    @if (!empty($msj_success))
        alertify.success('{{ $msj_success }}');
    @endif

    @if (!empty($msj_warning))
        alertify.warning('{{ $msj_warning }}');
    @endif


    @if (!empty($msj_danger))
        alertify.error('{{ $msj_danger }}');
    @endif


    @if (!empty($msj_info))
        alertify.notify('{{ $msj_info }}');
    @endif

</script>
