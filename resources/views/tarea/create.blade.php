@extends('layouts.app')

@section('subtitulo')
    Crear tarea
@endsection

@section('styles')

@endsection


@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Crear tarea</h3>
                    </div>
                    <div class="box-body">
                        <form id="fomulario" method="POST" action="{{ url('/admin/tarea') }}">
                            @csrf

                            <div class="form-group">
                                <label>Nombre</label>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre">
                            </div>

                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="descripcion" class="form-control" rows="3"
                                    placeholder="Descripción"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Fecha Fin</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="fecha_fin" type="date" class="form-control pull-right" id="datepicker">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control select2" style="width: 100%;">
                                    <option value="1" selected="selected">Activado</option>
                                    <option value="0">Desactivado</option>
                                </select>
                            </div>

                        </form>

                        <div class="box-footer">
                            <a href="{{ url('/admin/tarea') }}" class="btn btn-default">Cancelar</a>
                            <button form="fomulario" type="submit" class="btn btn-success  ">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
    </section>




@endsection




@section('scripts')


    //Date picker
    $('#datepicker').datepicker({
    autoclose: true
    })

@endsection
