@extends('layouts.app')

@section('subtitulo')
    Crear tarea
@endsection

@section('styles')

@endsection


@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Editar tarea</h3>
                    </div>
                    <div class="box-body">
                        <form id="fomulario" method="POST" action="{{ url('/admin/tarea/' . $tarea->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Nombre</label>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre"
                                    value="{{ $tarea->nombre }}">
                            </div>

                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="descripcion" class="form-control" rows="3"
                                    placeholder="Descripción"> {{ $tarea->descripcion }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Fecha Fin</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="fecha_fin" type="date" class="form-control pull-right" id="datepicker"  value="{{ $tarea->fecha }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control select2" style="width: 100%;">
                                    <option value="1" <?php echo $tarea->status == 1 ? 'selected' : ''; ?> >Activado</option>
                                    <option value="0" <?php echo $tarea->status == 0 ? 'selected' : ''; ?> >Desactivado</option>
                                </select>
                            </div>

                        </form>

                        <div class="box-footer">
                            <a href="{{ url('/admin/tarea') }}" class="btn btn-default">Cancelar</a>
                            <button form="fomulario" type="submit" class="btn btn-success  ">Modificar</button>
                        </div>
                    </div>
                </div>
            </div>
    </section>




@endsection



@section('scripts')

@endsection
