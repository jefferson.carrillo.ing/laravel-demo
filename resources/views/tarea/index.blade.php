@extends('layouts.app')

@section('subtitulo')
    Tareas
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/select.dataTables.min.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">

    <style>
        .estado_0 {
            background: grey;
        }

        .estado_1 {
            background: green;
        }

        .estado_2 {
            background: blue;
        }

        .estado_3 {
            background: orange;
        }

        .estado_4 {
            background: red;
        }

        div.container {
            max-width: 1200px;
        }

        .disabled{
            cursor: not-allowed;
        }
    </style>

@endsection



@section('content')



    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">

                        <h3 class="box-title">Gestor de tarea</h3>
                    </div>
                    <div class="box-body">
                        <table id="id_tabla_tarea" class="table table-striped table-bordered" style="width:100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>N°</th>
                                    <th>Estado</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Fecha fin</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php  $index=0;  @endphp
                                @foreach ($tareas as $tarea)
                                    @php  $index++;   @endphp
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>
                                            <small title="{{ $tarea->estado_info }}"
                                                class="label estado_{{ $tarea->id_estado }}">{{ $tarea->estado }}</small>
                                        </td>
                                        <td>{{ $tarea->nombre }}</td>
                                        <td>{{ $tarea->descripcion }}</td>
                                        <td>{{ $tarea->fecha_fin }}</td>

                                        @if ($tarea->status == 1)
                                            <td>Activado</td>
                                        @else
                                            <td>Desactivado</td>
                                        @endif
                                        <td>
                                            <form action="{{ url('admin/tarea/' . $tarea->id) }}" method="POST"
                                                class="pull-right">
                                                @csrf

                                                @if ($tarea->id_estado != 3)
                                                    <!--SOLO UANDO ESTA EN EJECUCION-->
                                                    <a href="{{ url('/admin/tarea/pausar/' . $tarea->id) }}"
                                                        title="PAUSAR"
                                                        class="btn btn-primary  margin-r-5 {{ $tarea->id_estado != 1 ? 'disabled' : '' }}">
                                                        <span class="fa fa-pause" />
                                                    </a>
                                                @else
                                                    <a href="{{ url('/admin/tarea/iniciar/' . $tarea->id) }}"
                                                        title="INICIAR" class="btn btn-success  margin-r-5 ">
                                                        <span class="fa fa-play" />
                                                    </a>
                                                @endif

                                                <!--SOLO UANDO ESTA EN EJECUCION-->
                                                <a href="{{ url('/admin/tarea/cancelar/' . $tarea->id) }}"
                                                    title="CANCELAR"
                                                    class="btn btn-primary  margin-r-5 {{ $tarea->id_estado != 1 ? 'disabled' : '' }}">
                                                    <span class="fa fa-close" />
                                                </a>

                                                <!--SI ESTA CANCELADA NO PUEDE SER MODIFICADA-->
                                                <a href="{{ url('/admin/tarea/' . $tarea->id . '/edit') }}"
                                                    title="EDITAR"
                                                    class="btn btn-warning   margin-r-5  {{ $tarea->id_estado == 4 ? 'disabled' : '' }}">
                                                    <span class="fa fa-edit" />
                                                </a>
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger   margin-r-5 " title="ELIMINAR">
                                                    <span class="fa fa-trash" /></button>

                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </section>










@endsection


@section('scripts')
    <script src="{{ asset('template/plugins/datatables/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.buttons.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.select.min.js') }} "></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>


    <script>
        $(document).ready(function() {
            $('#id_tabla_tarea').DataTable({
                'dom': "Bfrtip",
                "order": [[ 0, "asc" ]],
                'responsive': true,
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                'select': true,

                "language": {
                    "url": "{{ asset('template/plugins/datatables/Spanish.json') }}"
                },
                buttons: [{
                        text: "Refrescar",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.reload();
                        }
                    },
                    {
                        text: "Crear",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.href = "/admin/tarea/create";

                        }
                    }
                ]


            });

        });

    </script>
@endsection
