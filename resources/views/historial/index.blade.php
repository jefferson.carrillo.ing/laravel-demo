@extends('layouts.app')

@section('subtitulo')
  Historial
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/plugins/datatables/select.dataTables.min.css') }}">

    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">

    <style>
        .estado_0 {
            background: grey;
        }

        .estado_1 {
            background: green;
        }

        .estado_2 {
            background: blue;
        }

        .estado_3 {
            background: orange;
        }

        .estado_4 {
            background: red;
        }

        div.container {
            max-width: 1200px
        }

    </style>

@endsection


@section('content')



    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Historial de tareas</h3>
                    </div>
                    <div class="box-body">

                        <table id="id_tabla_historial" class="table table-striped table-bordered" style="width:100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>N°</th>
                                    <th>Tarea</th>
                                    <th>Estado</th>
                                    <th>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php  $index=0;  @endphp
                                @foreach ($historiales as $historial)
                                    @php  $index++;   @endphp
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>{{ $historial->tarea }}</td>
                                        <td>
                                            <small
                                                class="label estado_{{ $historial->id_estado }}">{{ $historial->estado }}</small>
                                        </td>
                                        <td>{{ $historial->fecha }}</td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </section>





@endsection


@section('scripts')
    <script src="{{ asset('template/plugins/datatables/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.buttons.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.select.min.js') }} "></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>


    <script>
        $(document).ready(function() {
            $('#id_tabla_historial').DataTable({
                'dom': "Bfrtip",

                'responsive': true,
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                'select': true,

                "language": {
                    "url": "{{ asset('template/plugins/datatables/Spanish.json') }}"
                },
                buttons: [{
                        text: "Refrescar",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.reload();
                        }
                    },
                    {
                        text: "Crear",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.href = "/admin/tarea/create";

                        }
                    }
                ]


            });

        });

    </script>
@endsection
