<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('template/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('template/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('template/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('template/plugins/iCheck/square/blue.css') }}">

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><b>{{ config('app.name', 'Laravel') }}</b> </a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Restaurar contraseña') }}</p>

            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group row">
                    <div class="form-group has-feedback">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            placeholder="{{ __('Correo electronico') }}" name="email" value="{{ old('email') }}"
                            required autocomplete="email" autofocus>

                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <div class="form-group has-feedback">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password" placeholder="{{ __('Contraseña') }}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row">
                    <div class="form-group has-feedback">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required autocomplete="new-password" placeholder="{{ __('Confirmar Contraseña') }}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                    </div>
                </div>




                <div class="row ">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-danger btn-block btn-flat"><i
                                class="fa  fa-check-circle"></i>
                            {{ __('Cambiar contraseña') }}
                        </button>
                    </div>
                </div>
            </form>
            <br>
            <div class="social-auth-links text-center">


                @if (Route::has('login'))
                    <a href="{{ route('login') }}" class="btn btn-block btn-primary btn-flat">
                        <i class="fa fa-user-circle"></i> {{ __('Login') }}
                    </a>
                @endif


                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="btn btn-block btn-success btn-flat">
                        <i class="fa fa-user-circle"></i> {{ __('Registrarse') }}
                    </a>
                @endif



            </div>


        </div>
    </div>

    <!-- jQuery 3 -->
    <script src="{{ asset('template/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('template/plugins/iCheck/icheck.min.js') }}"></script>

</body>

</html>
