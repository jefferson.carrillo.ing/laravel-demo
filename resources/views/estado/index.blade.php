@extends('layouts.app')

@section('subtitulo')
    Estado
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('template/plugins/datatables/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('template/plugins/datatables/select.dataTables.min.css') }}">

<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css">


@endsection


@section('content')



    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Gestor de estados</h3>
                    </div>
                    <div class="box-body">

                        <table id="id_tabla_estado" class="table table-striped table-bordered" style="width:100%">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th>N°</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Status</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php  $index=0;  @endphp
                                @foreach ($estados as $estado)
                                    @php  $index++;   @endphp
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>{{ $estado->nombre }}</td>
                                        <td>{{ $estado->descripcion }}</td>

                                        @if ($estado->status == 1)
                                            <td>Activado</td>
                                        @else
                                            <td>Desactivado</td>
                                        @endif
                                        <td>


                                            <form action="{{ url('admin/estado/' . $estado->id) }}" method="POST" class=" pull-left">
                                                <a href="{{ url('/admin/estado/' . $estado->id . '/edit') }}"
                                                    title="EDITAR" class="btn btn-warning">
                                                    <span class="fa fa-edit" />
                                                </a>

                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" title="ELIMINAR"> <span
                                                        class="fa fa-trash" /></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </section>





@endsection


@section('scripts')
    <script src="{{ asset('template/plugins/datatables/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.buttons.min.js') }} "></script>
    <script src="{{ asset('template/plugins/datatables/dataTables.select.min.js') }} "></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>


    <script>
        $(document).ready(function() {
            $('#id_tabla_estado').DataTable({
                'dom': "Bfrtip",

                'responsive': true,
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                'select': true,

                "language": {
                    "url": "{{ asset('template/plugins/datatables/Spanish.json') }}"
                },
                buttons: [{
                        text: "Refrescar",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.reload();
                        }
                    },
                    {
                        text: "Crear",
                        className: 'btn btn-primary',
                        action: function(e, dt, node, config) {
                            location.href="/admin/tarea/create";

                        }
                    }
                ]


            });

        });

    </script>
@endsection
