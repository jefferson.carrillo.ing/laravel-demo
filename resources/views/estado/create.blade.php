@extends('layouts.app')

@section('subtitulo')
    Crear Rol
@endsection

@section('styles')

@endsection


@section('content')


    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Crear estado</h3>
                    </div>
                    <div class="box-body">
                        <form id="fomulario" method="POST" action="{{ url('/admin/estado') }}">
                            @csrf

                            <div class="form-group">
                                <label>Nombre</label>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre">
                            </div>

                            <div class="form-group">
                                <label>Descripción</label>
                                <textarea name="descripcion" class="form-control" rows="3"
                                    placeholder="Descripción"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control select2" style="width: 100%;">
                                    <option value="1" selected="selected">Activado</option>
                                    <option value="0">Desactivado</option>
                                </select>
                            </div>

                        </form>

                        <div class="box-footer">
                            <a href="{{ url('/admin/estado') }}" class="btn btn-default">Cancelar</a>
                            <button form="fomulario" type="submit" class="btn btn-success  ">Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
    </section>




@endsection



@section('scripts')

@endsection
