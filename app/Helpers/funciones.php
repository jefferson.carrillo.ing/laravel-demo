<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('EsSuperAdmin')) {
    function  EsSuperAdmin()
    {
        return Auth::user()->id_rol == 1;
    }
}
