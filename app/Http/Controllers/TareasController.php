<?php

namespace App\Http\Controllers;

use App\Models\Estados;
use App\Models\Historiales;
use App\Models\Tareas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TareasController extends Controller
{

    private function GetData()
    {




        $id_usuario =  Auth::user()->id;
        $tareas = Tareas::select('*')->where('id_usuario',  $id_usuario)->orderBy('id', 'ASC')->get();
        $estados = Estados::select('id', 'nombre', 'descripcion')->where('status',  1)->orderBy('id', 'ASC')->get();

        // 1	Ejecutándose:
        // 2	Finalizada:
        // 3	Pausada:
        // 4	Cancelada:

        $fecha_actual = now()->toDateString();
        foreach ($tareas as   $tarea) {
            $fecha_fin =   $tarea->fecha_fin;

            $tarea->estado =      'Ninguno';
            $tarea->estado_info = 'No se ha establecido ningun estado.';
            foreach ($estados as   $estado) {
                switch (true) {
                    case $estado->id == 0: //Ninguno
                        $tarea->estado = $estado->nombre;
                        $tarea->estado_info = $estado->descripcion;
                        break;
                    case $estado->id == 1 &&  $fecha_fin < $fecha_actual && is_null($tarea->id_estado):  //Ejecutándose  tarea cuya fecha fin es menor a la fecha actual.
                        $tarea->id_estado = $estado->id;
                        $tarea->estado = $estado->nombre;
                        $tarea->estado_info = $estado->descripcion;
                        break;
                    case $estado->id == 2 &&   $fecha_fin >=  $fecha_actual && is_null($tarea->id_estado):  //Finalizada tarea cuya fecha fin es mayor o igual a la fecha actual.
                        $tarea->id_estado = $estado->id;
                        $tarea->estado = $estado->nombre;
                        $tarea->estado_info = $estado->descripcion;
                        break;
                    case $estado->id == 3  && $tarea->id_estado == 3: //Pausada tarea que estando en estado “ejecutándose” explícitamente fue pausada por el propietario de dicha tarea.
                        $tarea->id_estado = $estado->id;
                        $tarea->estado = $estado->nombre;
                        $tarea->estado_info = $estado->descripcion;
                        break;
                    case $estado->id == 4 && $tarea->id_estado == 4: //Cancelada  Tarea que estando en estado “Ejecutandose” fue cancelada explícitamente por el propietario.
                        $tarea->id_estado = $estado->id;
                        $tarea->estado = $estado->nombre;
                        $tarea->estado_info = $estado->descripcion;
                        break;
                }
            }
        }
        return $tareas;
    }

    public function index()
    {

        $tareas = $this->GetData();
        return    view('tarea.index')->with(compact('tareas'));
    }


    public function create()
    {
        return view('tarea.create');
    }


    public function store(Request $request)
    {
        $fecha_actual = now();
        $tarea = new Tareas();
        $tarea->id_usuario = Auth::user()->id;
        $tarea->nombre = $request->get('nombre');
        $tarea->descripcion = $request->get('descripcion');
        $tarea->fecha_fin = $request->get('fecha_fin');
        $tarea->status = $request->get('status');

        $tarea->created_at =  $fecha_actual;
        $tarea->updated_at =  $fecha_actual;
        $tarea->save();

        $id_estado =  $tarea->fecha_fin < $fecha_actual ? 1 : 2;
        $this->RegistrarHistorial($id_estado,  $tarea->id, $fecha_actual);

        $msj_success = "Tarea (" . $tarea->nombre . ") ingresada correctamente.";
        $tareas = $this->GetData();
        return view('tarea.index')->with(compact('tareas', 'msj_success'));
    }


    public function show(Tareas $tareas)
    {

        $tareas =  Tareas::select('t.nombre', 'e.nombre as estado_name')->join('estados as e', 't.id_estado', 'e.id')->get();
        return $tareas;
    }


    public function edit($id)
    {
        $fecha_actual = now();
        $tarea =  Tareas::find($id);

        $validacion = $this->validaciones($tarea, $fecha_actual, 'edit');
        if ($validacion != null) {
            return   $validacion;
        } else {
            return  view('tarea.edit', compact('tarea'));
        }
    }


    public function update(Request $request, $id)

    {

        $fecha_actual = now();
        $tarea =  Tareas::find($id);

        $validacion = $this->validaciones($tarea, $fecha_actual, 'update');
        if ($validacion != null) {
            return   $validacion;
        } else {
            $tarea->nombre = $request->get('nombre');
            $tarea->descripcion = $request->get('descripcion');
            $tarea->fecha_fin = $request->get('fecha_fin');
            $tarea->updated_at =  $fecha_actual;
            $tarea->save();

            $id_estado = $tarea->fecha_fin < $fecha_actual ? 1 : 2;

            $id_estado = is_null($tarea->id_estado) ? $id_estado : $tarea->id_estado;
            $this->RegistrarHistorial($id_estado,  $tarea->id, $fecha_actual);

            $msj_success = "Tarea (" . $tarea->nombre . ") modificado correctamente.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_success'));
        }
    }



    public function validaciones($tarea, $fecha_actual, $metodo)
    {

        if ($tarea->id_estado == 4) { //una tarea en estado Cancelada no puede modificar su estado.
            $msj_warning = "Tarea (" . $tarea->nombre . ") fue cancelada y no puede ser modificada.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_warning'));
        } else if ($tarea->id_estado == 3 && $metodo == 'cancelar') { //una tarea en estado Cancelada no puede modificar su estado.
            $msj_warning = "Tarea (" . $tarea->nombre . ") no puede ser cancelada por que se encuentra pausada.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_warning'));
        } else if ($tarea->id_estado == 3 &&  !($tarea->fecha_fin < $fecha_actual) && !($metodo == 'edit' || $metodo == 'update')) { //este tipo de tareas pueden volver al estado “Ejecutandose” siempre y cuando la fecha de fin sea menor a la fecha actua
            $msj_warning = "Tarea (" . $tarea->nombre . ") no puede volver a ser iniciada ya que la fecha fin es mayor a la actual.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_warning'));
        } else {
            return null;
        }
    }



    public function destroy($id)
    {
        $tarea =  Tareas::find($id);

        $historiales =  Historiales::select('id')->where('id_tarea', $tarea->id)->get();
        $cant_hist = count($historiales);
        if ($cant_hist == 0) {
            $tarea->delete();
            $msj_success = "Tarea (" . $tarea->nombre . ") eliminada correctamente.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_success'));
        } else {
            $msj_danger = "Tarea (" . $tarea->nombre . ") no puede ser eliminada por que posee " . $cant_hist . " registros relacionados.";
            $tareas = $this->GetData();
            return view('tarea.index')->with(compact('tareas', 'msj_danger'));
        }
    }


    public function cancelar($id)
    {
        $fecha_actual = now();
        $tarea =  Tareas::find($id);

        $validacion = $this->validaciones($tarea, $fecha_actual, 'cancelar');
        if ($validacion != null) {
            return   $validacion;
        } else {

            $tarea->id_estado = 4; //cancelado
            $tarea->save();
            $this->RegistrarHistorial($tarea->id_estado,  $tarea->id, $fecha_actual);

            $tareas = $this->GetData();
            $msj_success = "Tarea (" . $tarea->nombre . ") cancelada correctamente.";
            return view('tarea.index')->with(compact('tareas', 'msj_success'));
        }
    }

    public function pausar($id)
    {

        $fecha_actual = now();
        $tarea =  Tareas::find($id);

        $validacion = $this->validaciones($tarea, $fecha_actual, 'pausar');
        if ($validacion != null) {
            return   $validacion;
        } else {
            $tarea->id_estado = 3; //cancelado
            $tarea->updated_at =  $fecha_actual;
            $tarea->save();
            $this->RegistrarHistorial($tarea->id_estado,  $tarea->id, $fecha_actual);

            $tareas = $this->GetData();
            $msj_success = "Tarea (" . $tarea->nombre . ") pausada correctamente.";
            return view('tarea.index')->with(compact('tareas', 'msj_success'));
        }
    }

    public function iniciar($id)
    {

        $fecha_actual = now();
        $tarea =  Tareas::find($id);

        $validacion = $this->validaciones($tarea, $fecha_actual, 'iniciar');
        if ($validacion != null) {
            return   $validacion;
        } else {
            $tarea->id_estado = null;
            $tarea->updated_at =  $fecha_actual;
            $tarea->save();

            $id_estado =  $tarea->fecha_fin < $fecha_actual ? 1 : 2;
            $this->RegistrarHistorial($id_estado,  $tarea->id, $fecha_actual);

            $tareas = $this->GetData();
            $msj_success = "Tarea (" . $tarea->nombre . ") se esta ejecutando correctamente.";
            return view('tarea.index')->with(compact('tareas', 'msj_success'));
        }
    }


    public function RegistrarHistorial($id_estado, $id_tarea, $fecha_actual)
    {


        $fecha_actual =  $fecha_actual;

        $historial = new Historiales();
        $historial->id_usuario = Auth::user()->id;
        $historial->id_estado = $id_estado;
        $historial->id_tarea = $id_tarea;
        $historial->fecha =  $fecha_actual;
        $historial->created_at =  $fecha_actual;
        $historial->updated_at =  $fecha_actual;
        $historial->save();
    }
}
