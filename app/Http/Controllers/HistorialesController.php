<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HistorialesController extends Controller
{

    private function GetData()
    {
        $id_usuario =  Auth::user()->id;
        $historiales = DB::table('historiales as h')->select('h.id','t.nombre as tarea' , 'e.nombre as estado' , 'e.id as id_estado', 'h.fecha')
        ->join('tareas as t', 'h.id_tarea', 't.id')
        ->join('estados as e', 'h.id_estado', 'e.id')
        ->where('h.id_usuario', $id_usuario)
        ->orderBy('h.id', 'asc')
        ->get();
        return  $historiales;
    }

    public function index()
    {
        $historiales = $this->GetData();
        return view('historial.index', compact('historiales'));
    }



}
