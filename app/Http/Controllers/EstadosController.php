<?php

namespace App\Http\Controllers;

use App\Models\Estados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EstadosController extends Controller
{

    private function GetData()
    {
        $estados = Estados::all();
        return  $estados;
    }

    public function index()
    {

        $estados = $this->GetData();
        return view('estado.index', compact('estados'));
    }


    public function create()
    {
        return view('estado.create');
    }


    public function store(Request $request)
    {
        $estado = new Estados();
        $estado->nombre = $request->get('nombre');
        $estado->descripcion = $request->get('descripcion');
        $estado->status = $request->get('status');

        $fecha_actual = now();
        $estado->created_at =  $fecha_actual;
        $estado->updated_at =  $fecha_actual;
        $estado->save();
        $msj_success = "Estado (" . $estado->nombre . ") ingresado correctamente.";
        $estados = $this->GetData();
        return view('estado.index')->with(compact('estados', 'msj_success'));
    }


    public function show(Estados $estados)
    {
    }


    public function edit($id)
    {
        $estado =  Estados::find($id);

        return view('estado.edit', compact('estado'));
    }


    public function update(Request $request, $id)

    {
        $estado =  Estados::find($id);
        $estado->nombre = $request->get('nombre');
        $estado->descripcion = $request->get('descripcion');
        $fecha_actual = now();
        $estado->updated_at =  $fecha_actual;
        $estado->save();

        $msj_success = "Estado (" . $estado->nombre . ") modificado correctamente.";
        $estados = $this->GetData();
        return view('estado.index')->with(compact('estados', 'msj_success'));
    }


    public function destroy($id)
    {
        $estado =  Estados::find($id);
        $estado->delete();

        $msj_success = "Estado (" . $estado->nombre . ") eliminado correctamente.";
        $estados = $this->GetData();
        return view('estado.index')->with(compact('estados', 'msj_success'));
    }
}
