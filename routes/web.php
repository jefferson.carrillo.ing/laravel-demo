<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EstadosController;
use App\Http\Controllers\HistorialesController;
use App\Http\Controllers\TareasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(); //Logearse al sistema


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    Route::resource('estado', EstadosController::class)->middleware('superadministrador');




    Route::resource('tarea', TareasController::class);
    Route::get('tarea/pausar/{id}', [TareasController::class, 'pausar']);
    Route::get('tarea/cancelar/{id}', [TareasController::class, 'cancelar']);
    Route::get('tarea/iniciar/{id}', [TareasController::class, 'iniciar']);
    Route::get('historial', [HistorialesController::class, 'index']);
});
